import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'this title';
  subtitle = 'test';
  username = '';

  list = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get('http://localhost:3000/api/products')
      .subscribe((res: any[]) => {
        this.list = res
      })
  }

  handleSubmit(evt) {
    evt.preventDefault()
    console.log(this.username)
  }
}
