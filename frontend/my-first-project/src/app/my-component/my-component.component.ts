import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.sass']
})
export class MyComponentComponent implements OnInit {
  @Input()
  item: any

  count: number = 0

  constructor() { }

  ngOnInit(): void {}

  handleFunction(evt) {
    this.count ++;
  }
  handleDelete(evt) {
    const result = confirm('Are you sure you want to delete?')
    if (result) {
      console.log('delete item', this.item._id)
    }
  }

}
