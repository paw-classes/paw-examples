function init() {
	const menuButton = document.querySelector('.menu-button');
	const menu = document.querySelector('.aside-menu');

	menuButton.addEventListener('click', () => {
		menu.classList.toggle('is-open');
	});
}

init();