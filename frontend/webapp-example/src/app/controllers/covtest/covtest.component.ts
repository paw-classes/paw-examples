import { Component, OnInit } from '@angular/core';
import { CovidtestsService } from 'src/app/services/covidtests.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-covtest',
  templateUrl: './covtest.component.html',
  styleUrls: ['./covtest.component.sass']
})
export class CovtestComponent implements OnInit {
  covtest: any
  constructor(public covtests: CovidtestsService, public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.covtests
      .getTestById(this.route.snapshot.params['covtestId'])
      .subscribe((result) => {
        this.covtest = result
      })
  }

}
