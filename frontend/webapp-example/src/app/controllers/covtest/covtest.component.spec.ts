import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovtestComponent } from './covtest.component';

describe('CovtestComponent', () => {
  let component: CovtestComponent;
  let fixture: ComponentFixture<CovtestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovtestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovtestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
