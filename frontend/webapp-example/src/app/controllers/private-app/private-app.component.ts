import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-private-app',
  templateUrl: './private-app.component.html',
  styleUrls: ['./private-app.component.sass']
})
export class PrivateAppComponent implements OnInit {
  user: any
  file: any
  constructor(public sessionService: SessionService, public router: Router, public http: HttpClient) { }

  ngOnInit(): void {
    this.sessionService.me().subscribe((user) => {
      this.user = user;
      if (!this.user) {
        const options = this.sessionService.expired ? { queryParams: { expired: 'true' } } : undefined
        this.router.navigate(['/login'], options);
      }
    })
  }

  onFileSelected(e) {
    console.log(e.target.files)
    this.file = (e.target.files || [])[0]
  }

  handleSubmit(e) {
    e.preventDefault()
    const formData = new FormData()
    formData.append('image', this.file)
    this.http.put('/api/events/5ecfb9c85eaa8fc5aef8a70a/image', formData)
    .subscribe(() => {
      console.log('ok')
    })
  }

}
