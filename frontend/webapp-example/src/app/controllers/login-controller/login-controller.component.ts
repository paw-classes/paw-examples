import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-controller',
  templateUrl: './login-controller.component.html',
  styleUrls: ['./login-controller.component.sass']
})
export class LoginControllerComponent implements OnInit {
  username: String
  password: String

  errors: String

  constructor(public sessionService: SessionService, public router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe((params) => {
        if (params.expired) {
          this.errors = 'Your session was expired'
        }
      })
  }

  handleSubmit(event): void {
    event.preventDefault()
    this.errors = ''
    this.sessionService.login(this.username, this.password)
    .subscribe(
      () => {
        this.router.navigate(['/'])
      },
      (error) => {
        if (error.status === 401) {
          this.errors = 'Invalid credentials.'
        } else {
          this.errors = error.message
        }
      }
    )
  }

}
