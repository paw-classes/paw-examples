import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CovtestsComponent } from './covtests.component';

describe('CovtestsComponent', () => {
  let component: CovtestsComponent;
  let fixture: ComponentFixture<CovtestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CovtestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CovtestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
