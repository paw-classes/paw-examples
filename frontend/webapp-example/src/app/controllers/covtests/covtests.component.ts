import { Component, OnInit } from '@angular/core';
import { CovidtestsService } from 'src/app/services/covidtests.service';

@Component({
  selector: 'app-covtests',
  templateUrl: './covtests.component.html',
  styleUrls: ['./covtests.component.sass']
})
export class CovtestsComponent implements OnInit {
  tests = null
  constructor(public covtests: CovidtestsService) { }

  ngOnInit(): void {
    this.covtests
      .getTests()
      .subscribe((list) => {
        this.tests = list
      })
  }

}
