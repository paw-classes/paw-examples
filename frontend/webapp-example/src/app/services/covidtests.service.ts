import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from './../../environments/environment';

const API_URL = environment.apiUrl;

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  }),
  withCredentials: true,
};

@Injectable({
  providedIn: 'root'
})
export class CovidtestsService {

  constructor(public http: HttpClient) { }

  getTests(): Observable<any> {
    // https://codecraft.tv/courses/angular/http/http-with-observables/
    return this.http.get(`${ API_URL }/lab-tests`, httpOptions)
  }
  getTestById(testId: string): Observable<any> {
    return this.http.get(`${ API_URL }/lab-tests/${ testId }`, httpOptions)
  }
}
