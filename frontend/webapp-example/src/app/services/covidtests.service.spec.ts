import { TestBed } from '@angular/core/testing';

import { CovidtestsService } from './covidtests.service';

describe('CovidtestsService', () => {
  let service: CovidtestsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CovidtestsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
