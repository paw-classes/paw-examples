import { Component, OnInit, Input } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  @Input()
  user: any
  constructor(public sessionService: SessionService) { }

  ngOnInit(): void {
  }

  logout() {
    this.sessionService.logout()
  }

}
