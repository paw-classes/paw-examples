import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginControllerComponent } from './controllers/login-controller/login-controller.component';
import { PrivateAppComponent } from './controllers/private-app/private-app.component';
import { DashboardComponent } from './controllers/dashboard/dashboard.component';
import { CovtestsComponent } from './controllers/covtests/covtests.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CovtestComponent } from './controllers/covtest/covtest.component';
import { SessionLostInterceptor } from './interceptors/session-lost.interceptor';
import { HeaderInterceptor } from './interceptors/header.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    LoginControllerComponent,
    PrivateAppComponent,
    DashboardComponent,
    CovtestsComponent,
    NavbarComponent,
    CovtestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SessionLostInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeaderInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
