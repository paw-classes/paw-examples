import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginControllerComponent } from './controllers/login-controller/login-controller.component';
import { PrivateAppComponent } from './controllers/private-app/private-app.component';
import { CovtestsComponent } from './controllers/covtests/covtests.component';
import { DashboardComponent } from './controllers/dashboard/dashboard.component';
import { CovtestComponent } from './controllers/covtest/covtest.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginControllerComponent
  },
  {
    path: '',
    component: PrivateAppComponent,
    children: [
      {
        path: 'covtests',
        component: CovtestsComponent
      },
      {
        path: 'covtests/:covtestId',
        component: CovtestComponent
      },
      {
        path: '',
        component: DashboardComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
