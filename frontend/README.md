# Running the examples using VS Code

1. Install the **Live Server** Plugin ([Plugin Link](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer))

2. Install the **Live Sass Compiler** Plugin ([Plugin Link](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass))

3. Run the **Live Server** on the **root folder** to access all the examples. If you want to run a specific example, run the Live Server within the **specific example folder**.

4. Run the **Live Sass Compiler**, so you can edit the *.sass* files and get the corresponding *.css* files automatically generated.