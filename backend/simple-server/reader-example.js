const read = require('./myReader')

read('./tests.txt')
	.then((data) => {
		console.log('from promise', data)
	})
	.catch((err) => {
		console.error('err', err)
	})
