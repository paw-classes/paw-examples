var mongoose = require("mongoose");
var Employee = require("../models/Employee");

var employeeController = {};

employeeController.list = async function (req, res) {
  // TODO
  try {
    const filters = {}
    if (req.query.salary) {
      filters.salary = {
        $gte: parseFloat(req.query.salary)
      }
    }
    if (req.query.position) {
      filters.position = req.query.position
    }
    const employeesList = await Employee.find(filters)
    res.render('employees', { employees: employeesList, filters: req.query })
  } catch (e) {
    res.render('employees', { employees: [] })
  }
};

employeeController.show = async function (req, res) {
  // TODO
  console.log('ID', req.params.id)
  const employee = await Employee.findById(req.params.id)
  res.render('employees/show', { employee: employee || {} })
};

employeeController.create = function (req, res) {
  res.render("employees/create");
};

employeeController.save = async function (req, res) {
  // TODO
  try {
    const result = await Employee.create(req.body)
    res.redirect(`/employees/show/${ result._id }`)
  } catch(e) {
    res.send('error')
  }
};

employeeController.edit = async function (req, res) {
  // TODO
  const employee = await Employee.findById(req.params.id)
  res.render('employees/edit', {
    employee
  })
};

employeeController.update = async function (req, res) {
  // TODO
  const result = await Employee.findByIdAndUpdate(req.params.id, req.body)
  res.redirect(`/employees/show/${ result._id }`)
};

employeeController.delete = async function (req, res) {
  // TODO
  await Employee.findByIdAndDelete(req.params.id)
  res.redirect('/employees')
};





module.exports = employeeController;
