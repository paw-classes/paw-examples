const express = require('express')

const path = require('path')
const fs = require('fs')

const PORT = process.env.PORT || 3000
const app = express()

const indexRouter = require('./routes/index')

app
	.use(express.static('public'))

	.use(express.json())
	.use(express.urlencoded({ extended: true }))

	.set('view engine', 'ejs')

	.use('/', indexRouter)

	.get('/reviews', (req, res) => {
		const dbPath = path.resolve('db', 'posts.json')
		const posts = JSON.parse(fs.readFileSync(dbPath))
		res.render('pages/reviews', { list: posts })
	})
	.get('/reviews/:reviewId', (req, res) => {
		const dbPath = path.resolve('db', 'posts.json')
		const posts = JSON.parse(fs.readFileSync(dbPath))
		res.render('pages/reviewDetails', { item: posts[req.params.reviewId] })
	})

	.listen(PORT, () => {
		console.log(`server started on http://localhost:${ PORT }`)
	})