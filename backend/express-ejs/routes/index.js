const express = require('express')
const fetch = require('node-fetch')

const path = require('path')
const fs = require('fs')

const router = express.Router()

router.get('/', async (req, res) => {
	// Demo of fetch request with express rest api
	// const response = await fetch('http://localhost:3001/api/products')
	// const data = await response.json()
	// console.log(data)
	const dbPath = path.resolve('db', 'posts.json')
	const dataRaw = fs.readFileSync(dbPath) || '[]'
	const data = JSON.parse(dataRaw.toString())
	const result = data.reduce((resultObj, book) => {
		if (!resultObj[book.grupo]) {
			resultObj[book.grupo] = []
		}
		resultObj[book.grupo].push(book)
		return resultObj
	}, {})
	console.log(result)
	res.render(
		'pages/homepage',
		{
			list: data.reverse().slice(0, 5)
		}
	)
})

router.post('/', (req, res) => {
	const dbPath = path.resolve('db', 'posts.json')
	const dataRaw = fs.readFileSync(dbPath) || '[]'
	const data = JSON.parse(dataRaw.toString())
	data.push(req.body)
	fs.writeFileSync(dbPath, JSON.stringify(data))
	res.render('pages/homepage', {
		success: true,
		list: data.reverse()
	})
})

module.exports = router
