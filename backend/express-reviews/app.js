const express = require('express')
const multer  = require('multer')

const path = require('path')

const mongoose = require('mongoose')

const Review = require('./models/Review')

mongoose.Promise = global.Promise

const PORT = process.env.PORT || 3000
const MONGO_DB_HOST = process.env.MONGO_DB_HOST || 'localhost'
const MONGO_BD_PORT = process.env.MONGO_BD_PORT || 27017
const MONGO_DB_NAME = process.env.MONGO_DB_NAME || 'demo'

mongoose
	.connect(
		`mongodb://${ MONGO_DB_HOST }:${ MONGO_BD_PORT }/${ MONGO_DB_NAME }`,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true,
		}
	)
	.then((mongoose) => {
		console.log('connected to mongo')
	})
	.catch(console.error)

const app = express()
const upload = multer({
	dest: path.resolve('public', 'images')
})

app
	.set('view engine', 'ejs')
	
	.use(express.static('public'))
	.use(express.json())
	.use(express.urlencoded({ extended: true }))

	.get('/', async (req, res) => {
		const reviews = await Review.find()
		res.render('pages/homepage', { reviews })
	})

	.post('/', upload.single('photo'), async (req, res) => {
		try {
			const reviewData = {
				...req.body,
				image: req.file ? `/images/${ req.file.filename }` : '',
				read: req.body.read === 'true' ? true : false
			}
			const result = await new Review(reviewData).save()
			res.render('pages/homepage', { success: true })
		} catch (e) {
			console.log(e)
			res.render('pages/homepage', { error: true })
		}
	})

	.get('/reviews', async (req, res) => {
		console.log(req.query)
		const reviews = await Review
			.find(req.query)
			.sort({
				'updated_at': 'desc'
			})
			// .sort('updated_at', 'asc')
		// console.log(reviews)
		res.render(
			'pages/reviews',
			{
				reviews,
				user: req.params.email
			})
	})
	.get('/reviews/:reviewSlug', async (req, res) => {
		console.log(req.params)
		const reviews = await Review
			.find({
				slug: req.params.reviewSlug
			})
			// .sort('updated_at', 'asc')
		// console.log(reviews)
		res.render(
			'pages/review',
			{
				reviews,
				user: req.params.email
			})
	})

	.listen(PORT, () => {
		console.log(`Server start at http://localhost:${ PORT }`)
	})