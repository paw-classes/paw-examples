const mongoose = require('mongoose')
const slug = require('slug')

const reviewSchema = new mongoose.Schema({

	book: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true
	},
	read: Boolean,
	image: String,
	slug: String,
	review: String,
	updated_at: { type: Date, default: Date.now },
})

reviewSchema.pre('save', function(next) {
	this.slug = slug(this.book)
	next()
})

module.exports = mongoose.model('Review', reviewSchema)