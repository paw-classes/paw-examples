require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const fetch = require('node-fetch')

const cors = require('cors')
const cookieParser = require('cookie-parser')

const swaggerUi = require('swagger-ui-express')
const swaggerJSDoc = require('swagger-jsdoc')

const apiRouter = require('./api')

const app = express()
mongoose.Promise = global.Promise


const options = { // line 27
	swaggerDefinition: {
		info: {
			title: 'swagger-express-jsdoc', // Title (required)
			version: '1.0.0', // Version (required)
		},
		basePath: '/api'
	},
	apis: [
		'./api/index.js',
		'./api/products-router.js'
	], // Path to the API docs
}
const swaggerSpec = swaggerJSDoc(options)

// Object destructuring ES6
const {
	PORT = 3000,
	MONGO_DB_HOST = 'localhost',
	MONGO_BD_PORT = 27017,
	MONGO_DB_NAME = 'demo2'
} = process.env

mongoose
	.connect(
		`mongodb://${ MONGO_DB_HOST }:${ MONGO_BD_PORT }/${ MONGO_DB_NAME }`,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false
		}
	)
	.then((mongoose) => {
		console.log('connected to mongo')
	})
	.catch(console.error)

app.set('view engine', 'ejs')
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
// prevent favicon error
app.use((req, res, next) => {
	if (req.path.endsWith('favicon.ico')) {
		res.status(404)
		res.send(null)
	} else {
		next()
	}
})
app.use('/api', cors(), apiRouter)

app.get('/api-docs.json', function (req, res) { // line 41
	res.setHeader('Content-Type', 'application/json');
	res.send(swaggerSpec);
})

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec))

app.get('/', (req, res) => {
	fetch(`http://localhost:${ PORT }/api/products`)
		.then((result) => result.json())
		.then((result) => {
			// console.log(result)
			res.render('homepage', { products: result })
		})
})
app.get('/:productId', async (req, res) => {
	const product = await fetch(`http://localhost:${ PORT }/api/products/${ req.params.productId }`)
		.then((result) => result.json())
	// TODO: print related products

	res.render('productDetail', { product: product, relatedProduct: [] })
})

app.listen(PORT, () => {
	console.log(`Server started on http://localhost:${ PORT }`)
})