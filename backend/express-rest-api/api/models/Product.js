const mongoose = require('mongoose')
require('./User')

const productSchema = new mongoose.Schema({

	name: String,
	description: String,
	quantity: Number,
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		default: null
	},
	updated_at: { type: Date, default: Date.now },
})

module.exports = mongoose.model('Product', productSchema)