const Product = require('../models/Product')

const createProduct = async (req, res) => {
	const productData = req.body
	console.log('product data')
	const result = await new Product(productData).save()
	res.send(result)
}
const getAllProducts = async (req, res) => {
	const productList = await Product.find().populate('user')
	res.send(productList)
}

const getProductById = async (req, res) => {
	try {
		const product = await Product
			.findById(req.params.productId)
			.catch((e) => {
				return null
			})
		res.send(product)
	} catch (e) {
		console.error(e)
		res.status(404)
		res.send(null)
	}
}
const updateProduct = async (req, res, next) => {
	console.log("Product set by setProductById", req.product)
	Product.findByIdAndUpdate(
		req.params.productId,
		req.body,
		{ new: true },
		function(err, product) {
			if (err) {
				next(err)
			} else {
				res.json(product)
			}
		}
	)
	// const oldProduct = await Product.findByIdAndUpdate(
	// 	req.params.productId,
	// 	req.body
	// )
	// const newProduct = await Product.findById(req.params.productId)
	// res.send({
	// 	old: oldProduct,
	// 	new: newProduct
	// })
}

const deleteProduct  = async (req, res) => {
	const deleteProduct = await Product.findByIdAndRemove(req.params.productId)
	res.send(deleteProduct)
}

const setProductById = async (req, _, next) => {
	try {
		const product = await Product
			.findById(req.params.productId)
			.catch((e) => {
				return {}
			})
		if (product) {
			req.product = product
			next()
		} else {
			next(new Error('not found'))
		}
	} catch (e) {
		next(e)
	}
}

module.exports = {
	createProduct,
	getAllProducts,
	getProductById,
	updateProduct,
	deleteProduct,
	setProductById
}