const express = require('express')

const productsController = require('../controllers/productsController')

const productsRouter = express.Router()

/**
 * @swagger
 * /products:
 *   get:
 *     description: Returns all products
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: products
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Product'
 */

productsRouter.get('/products/', productsController.getAllProducts)
productsRouter.get('/products/:productId', productsController.getProductById)

productsRouter.post('/products/', productsController.createProduct)
productsRouter.put('/products/:productId', productsController.updateProduct)

productsRouter.delete('/products/:productId', productsController.deleteProduct)

// this will set the product on req.product - for any route that contains
productsRouter.param('productId', productsController.setProductById)

module.exports = productsRouter
