const express = require('express')
const sessionRouter = require('./routes/session')
const testRouter = require('./routes/test')

const apiRouter = express.Router()

apiRouter.get('/', (req, res) => {
	res.json({
		status: 'ok'
	})
})

apiRouter.use(sessionRouter)
apiRouter.use(testRouter)

module.exports = apiRouter
