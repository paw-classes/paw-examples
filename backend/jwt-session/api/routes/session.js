const express = require('express')
const jwt = require('jsonwebtoken')

const sessionRouter = express.Router()

const SESSION_EXP = 60000
const {
	JWT_SECRET = 'this is for development'
} = process.env
const userJSON = {
	username: 'edgar',
	password: 'pass123',
	role: 'ADMIN' // 'REGULAR', 'EXTERNAL'
}
sessionRouter.post('/login', (req, res, next) => {

	// const user = User.find({ username: req.body.username })
	// validate password: user.password === req.body.password
	const jwtToken = jwt.sign(userJSON, JWT_SECRET)
	res.cookie(
		'session',
		jwtToken,
		{
			expires: new Date(Date.now() + SESSION_EXP),
			httpOnly: true
		}
	)
	res.json({
		userJSON,
		password: undefined
	})
})

sessionRouter.get('/me', (req, res, next) => {
	res.json(req.user)
})

sessionRouter.post('/logout', (req, res, next) => {
	res.clearCookie('session')
	res.json({ success: 'true' })
})

module.exports = sessionRouter