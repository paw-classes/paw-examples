const express = require('express')

const authorize = require('../middleware/authorize')

const testRouter = express.Router()

const mockData = [
	{ name: 'a', id: 'a' },
	{ name: 'b', id: 'b' }
]

testRouter.get('/list', (req, res) => {
	console.log(req.user)
	res.json(mockData)
})
testRouter.get('/list/:itemId', (req, res) => {
	console.log(req.user)
	res.json(mockData.find(({ id }) => id === req.params.itemId))
})
testRouter.post('/list', authorize(['REGULAR']), (req, res) => {
	console.log(req.body)
	res.json({ created:'ok' })
})

module.exports = testRouter