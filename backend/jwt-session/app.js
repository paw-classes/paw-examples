require('dotenv').config()
const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')

const apiRouter = require('./api')
const sessionMiddleware = require('./api/middleware/session')

const {
	PORT = 3000
} = process.env

const app = express()

app
	// Setup application to work with cors and body-parser
	.use(cors({
		credentials: true,
		origin: 'http://localhost:4200'
	}))
	.use(express.json())
	.use(express.urlencoded({ extended: true }))

	// Setup cookie parser
	.use(cookieParser())

	.use((req, res, next) => {
		console.log(req.cookies)
		next()
	})

	// Setup session middleware
	.use(sessionMiddleware)

	// Set API Router at /api endpoint
	.use('/api', apiRouter)

	// Start the server
	.listen(PORT, () => {
		console.log(`server started at http://localhost:${ PORT }`)
	})

