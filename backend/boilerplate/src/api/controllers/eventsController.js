const Event = require("../models/event")
const shortId = require('shortid')

const createFilters = (query, sessionUser) => {
    const { sort, date, code } = query
    const [startDate, endDate] = date || []
    const filter = {}
    const sortObject = {}
    if (startDate && endDate) {
        filter.date = {
            $gt: new Date(startDate) || undefined,
            $lt: new Date(endDate) || undefined,
        }
    }
    if (code) {
        filter.code = code
    }
    if (sessionUser && sessionUser.role === "EXTERNAL") {
        filter.user = sessionUser._id
    }
    if (sort) {
        const [field, direction] = sort.split(',')
        sortObject[field] = direction === 'asc' ? 1 : -1 
    }
    return [filter, sort]
}

const find = async (req, res, next) => {
    const [filter, sort] = createFilters(req.query, req.sessionUser)
    const events = await Event.find(filter).sort(sort)//.populate("user")
    res.json(events)
}

const findOne = async (req, res, next) => {
    const eventId = req.params.eventId
    const filter = { _id: eventId }
    if (req.sessionUser && req.sessionUser.role === "EXTERNAL") {
        filter.user = req.sessionUser._id
    }
    const event = await Event.findOne(filter).populate("user").catch(next)
    if (!event) {
        next({
            status: 404,
            message: `Cannot find the event with id ${eventId}`,
        })
    } else {
        res.json(event)
    }
}

const create = async (req, res, next) => {
    const body = req.body
    const eventData = {
        ...body,
        date: new Date(body.date) || undefined,
        user: req.sessionUser._id,
        code: shortId.generate()
    }
    const event = await new Event(eventData).save().catch(next)
    res.json(event)
}

const updateOne = async (req, res, next) => {
    const body = req.body
    const eventId = req.params.eventId
    const eventData = {
        ...body,
        date: new Date(body.date) || undefined,
    }
    const result = await Event.findOneAndUpdate({ _id: eventId }, eventData, {
        new: true,
    }).catch(next)
    if (!result) {
        next({
            status: 400,
            message: `Cannot update the event for id ${eventId}`,
        })
    } else {
        res.json(result)
    }
}

const updateImage = async (req, res, next) => {
    const eventId = req.params.eventId
    const image = req.file
    const eventData = {
        image: `/uploads/${image.filename}`,
    }
    const result = await Event.findOneAndUpdate({ _id: eventId }, eventData, {
        new: true,
    }).catch(next)
    if (!result) {
        next({
            status: 400,
            message: `Cannot update the event for id ${eventId}`,
        })
    } else {
        res.json(result)
    }
}
const deleteOne = () => {}

const groupEventsPerDay = () => {
    return Event.aggregate([
        {
            $group: {
                _id: {
                    // https://docs.mongodb.com/manual/reference/operator/aggregation/dateToString/
                    $dateToString: { format: "%Y-%m-%d", date: "$date" },
                },
                total: { $sum: 1 },
            },
        },
        {
            $project: {
                date: "$_id",
                totalEvents: "$total",
                _id: false,
            },
        },
        {
            $sort: { date: 1 },
        },
    ]).catch((e) => {
        console.log(e)
        return []
    })
}

const countEvents = () => {
    return Event.countDocuments().catch((e) => {
        console.log(e)
        return 0
    })
}

const stats = (async = async (req, res, next) => {
    // https://mongoosejs.com/docs/api/aggregate.html
    // https://docs.mongodb.com/manual/reference/operator/aggregation/count/
    const [perDay, totalEvents] = await Promise.all([
        groupEventsPerDay(),
        countEvents(),
    ])

    res.json({
        perDay,
        totalEvents,
    })
})

module.exports = {
    stats,
    find,
    findOne,
    create,
    updateOne,
    updateImage,
    deleteOne,
}
