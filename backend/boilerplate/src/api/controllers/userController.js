const User = require('../models/user')

const find = async (req, res, next) => {
	const users = await User.find().catch(next)
	res.json(users)
}
const findById = async (req, res, next) => {
	const user = await User.findById(req.params.userId).catch(next)
	res.json(user)
}

const findByIdAndUpdateRole = async (req, res, next) => {
	const updatedUser = await User
		.findByIdAndUpdate(
			req.params.userId,
			{ role: req.body.role },
			{ new: true, runValidators: true },
		)
		.catch(next)
	if (updatedUser) {
		res.json(updatedUser)
	} else {
		next({
			message: 'User not found',
			status: 404
		})
	}
}

const create = async (req, res, next) => {
	const body = req.body
	const userData = {
		...body,
		role: 'EXTERNAL'
	}
	const user = await new User(userData).save().catch(next)
	res.json(user)
}

module.exports = {
	create,
	find,
	findById,
	findByIdAndUpdateRole
}