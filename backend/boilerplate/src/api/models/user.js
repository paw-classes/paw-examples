const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: true
	},
	lastName: {
		type: String,
		required: true
	},
	username: {
		type: String,
		unique: true,
		required: true
	},
	email: {
		type: String,
		unique: true,
		required: true
	},
	password: {
		type: String,
		required: true,
		minlength: 7,
		select: false // Prevent password from being populated
	},
	role: {
		type: String,
		enum: ['ADMIN', 'COLLABORATOR', 'EXTERNAL'],
		default: 'EXTERNAL',
		index: true
	},
	updatedAt: { type: Date, default: Date.now }
})

module.exports = mongoose.model('User', userSchema)