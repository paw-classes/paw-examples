const express = require('express')
const eventsController = require('../controllers/eventsController')
const upload = require('../middleware/upload')
const eventsRouter = express.Router()

eventsRouter.get('/events_stats', eventsController.stats)

eventsRouter.post('/events', eventsController.create)
eventsRouter.get('/events', eventsController.find)
eventsRouter.get('/events/:eventId', eventsController.findOne)
eventsRouter.put('/events/:eventId', eventsController.updateOne)

/**
 * @swagger
 * /events/{eventId}/image:
 *   put:
 *     description: Login to the application
 *     tags: [Events]
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: eventId
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the event to update
 *       - $ref: '#/parameters/image'
 *     responses:
 *       200: 
 *         $ref: '#/responses/LoginSuccess'
 */
eventsRouter.put('/events/:eventId/image', upload.single('image'), eventsController.updateImage)
eventsRouter.delete('/events/:eventId', eventsController.deleteOne)

module.exports = eventsRouter