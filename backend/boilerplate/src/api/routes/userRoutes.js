const express = require('express')
const userController = require('../controllers/userController')
const userRouter = express.Router()

/**
 * @swagger
 * /users:
 *   get:
 *     description: Returns a list of users
 *     tags: [Users]
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: List of Users
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             $ref: '#/definitions/User'
 */
userRouter.get('/users', userController.find)

/**
 * @swagger
 * /users/{userId}:
 *   get:
 *     description: Returns a specific user
 *     tags: [Users]
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: path
 *        name: userId
 *        schema:
 *          type: string
 *        required: true
 *        description: ID of the user to get
 *     responses:
 *       200:
 *         description: Health check
 *         schema:
 *           type: object
 *           $ref: '#/definitions/User'
 */
userRouter.get('/users/:userId', userController.findById)

/**
* @swagger
* /users/{userId}/role:
*   put:
*     description: Login to the application
*     tags: [Users]
*     produces:
*       - application/json
*     parameters:
*       - in: path
*         name: userId
*         schema:
*           type: string
*         required: true
*         description: ID of the user to get
*       - name: role
*         description: User new role.
*         in: formData
*         required: true
*         type: string
*     responses:
*       200:
*         description: Health check
*         schema:
*           type: object
*           $ref: '#/definitions/User'
*/
userRouter.put('/users/:userId/role', userController.findByIdAndUpdateRole)
userRouter.post('/users', userController.create)

module.exports = userRouter
