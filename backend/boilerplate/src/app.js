require('dotenv').config() // It must be the first line of code

const express = require('express')
const cookieParser = require('cookie-parser')
const cors = require('cors')

const path = require('path')

const mongoose = require('mongoose')


const api = require('./api')
const session = require('./api/middleware/session')
const User = require('./api/models/user')
const swagger = require('./api/swagger')

const app = express()

// Read values from environment variables
const PORT = process.env.APP_PORT
const MONGO_DB_HOST = process.env.MONGO_DB_HOST
const MONGO_DB_PORT = process.env.MONGO_DB_PORT
const MONGO_DB_DATABASE_NAME = process.env.MONGO_DB_DATABASE_NAME

// Connect to mongo DB
mongoose
    .connect(
        `mongodb://${ MONGO_DB_HOST }:${ MONGO_DB_PORT }/${ MONGO_DB_DATABASE_NAME }`,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
        }
    )
    .then(async (mongoose) => {
        console.log('connected to mongo')
        const adminUser = await User.findOne({ role: 'ADMIN' }).select('+password')
        if (!adminUser) {
            console.log('creating admin user')
            const adminUser = await new User({
                username: process.env.ADMIN_USERNAME,
                password: process.env.ADMIN_PASSWORD,
                firstName: process.env.ADMIN_FIRST_NAME,
                lastName: process.env.ADMIN_LAST_NAME,
                email: process.env.ADMIN_EMAIL,
                role: 'ADMIN'
            })
                .save()
                .catch(console.error)

            if (adminUser) {
                console.log('Admin created')
                console.table([adminUser.toJSON()])
            }
        } else {
            console.log('Admin:')
            console.table([adminUser.toJSON()])
        }
    })
    .catch(console.error)

// Setup public uploads folder
app.use('/uploads', express.static(path.join(__dirname, '../uploads')))

// Setup application to support body-parser
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Setup cookie parser
app.use(cookieParser())

// Setup session middleware
app.use(session)

// Set API Router at /api endpoint and enable cors
// // If you do not use angular proxy
// const whitelist = ['http://localhost:4200', 'http://localhost:3000', 'http://localhost']
// const corsOptions = {
//     credentials: true,
//     origin: function (origin, callback) {
//         console.log(origin)
//         if (!origin || whitelist.includes(origin)) {
//             callback(null, true)
//         } else {
//             callback(new Error('Not allowed by CORS'))
//         }
//     }
// }
app.use('/api', cors(), api)

// Set swagger router. The paths defined in the router are: /api-docs, /api-docs.json
app.use(swagger)

// Start the server
app.listen(PORT, () =>
    console.log(`Example app listening at http://localhost:${ PORT }`)
)

