# PAW Classes Examples 2020

This project aims to exemplify the material given in PAW course. You can find several examples that are distributed in the respective folders.

## Frontend Examples

All the frontend examples are distributed within the frontend folder

## backend Examples

All the backend examples are distributed within the backend folder
